#include <stdio.h>
#include <time.h>

char *testcase_description = "global arm64 atomics - stadd";

/* stadd a global variable, then ldr back */

#define cpu_relax() asm volatile("yield" ::: "memory")

typedef struct {
	union {
		long cnt;
		char pad[64];
	};
} atomic_t;

static atomic_t __attribute__ ((aligned (64))) test_data = {.cnt=0xabcdef};

//~1000/s on siryn
//#define NR_RELAX 350000UL
//~105/s on siryn
#define NR_RELAX 3500000UL
//~5/s on siryn
//#define NR_RELAX 70000000UL

#define RND_ARRAY_SIZE 1024

/* atomic add i to v->cnt */
static __always_inline void atomic_add(int i, atomic_t *v)
{
	asm volatile ("stadd %[i], %[v] \n" : [v] "+Q" (v->cnt) : [i] "r" (i) : "memory");
}

static __always_inline long atomic_read(atomic_t *v)
{
	long a;
	asm volatile ("ldr %[i], %[v] \n" : [i] "=r" (a) : [v] "Q" (v->cnt) : "memory");
	return a;
}

void testcase(unsigned long long *iterations, unsigned long nr)
{
	long a;
	size_t nr_relax = NR_RELAX;
	

	while (1) {
		atomic_add(1, &test_data);
		a = atomic_read(&test_data);	

		/* insert 'yield' to avoid tight loop */
		//printf("nr_relax: %lu, a: %lx\n", nr_relax, a);
		for (size_t i = 0; i < nr_relax; ++i) {
			cpu_relax();
		}
		(*iterations) += 1;
	}
}
