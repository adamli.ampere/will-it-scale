#include <stdio.h>

char *testcase_description = "global arm64 atomics - stadd";

/* stadd a global variable, then ldr back */

#define cpu_relax() asm volatile("yield" ::: "memory")

typedef struct {
	union {
		long val;
		char pad[64];
	};
} test_data_t;

static test_data_t __attribute__ ((aligned (64))) test_data = {.val=0xabcdef};

void testcase(unsigned long long *iterations, unsigned long nr)
{
	int a = 1, b = 0;
	//size_t nr_relax = 3500000; //~105/s on siryn
	size_t nr_relax = 70000000; //~5/s on siryn

	while (1) {
		asm volatile (
			"stadd  %[i], %[v] \n\t"
			"ldr  %[o], %[v] \n\t"
				: [v] "+Q" (test_data.val), [o] "+r" (b)
				: [i] "r" (a) 
				: "memory");

		//usleep(10000);
		//usleep(200000);

		for (size_t i = 0; i < nr_relax; ++i) {
			cpu_relax();
		}

		(*iterations) += 1;
	}
}
