#include <stdio.h>
#include <time.h>

char *testcase_description = "global arm64 atomics - stadd";

/* stadd a global variable, then ldr back */

#define cpu_relax() asm volatile("yield" ::: "memory")

typedef struct {
	union {
		long val;
		char pad[64];
	};
} test_data_t;

static test_data_t __attribute__ ((aligned (64))) test_data = {.val=0xabcdef};

//~105/s on siryn
//#define NR_RELAX 3500000UL
//~5/s on siryn
#define NR_RELAX 70000000UL

void testcase(unsigned long long *iterations, unsigned long nr)
{
	int a = 1, b = 0;
	size_t nr_relax = NR_RELAX; 
	int offset = 0;

	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	srand(ts.tv_sec * 1000000000 + ts.tv_nsec);

	while (1) {
		asm volatile (
			"stadd  %[i], %[v] \n\t"
			"ldr  %[o], %[v] \n\t"
				: [v] "+Q" (test_data.val), [o] "+r" (b)
				: [i] "r" (a) 
				: "memory");

		nr_relax = NR_RELAX; 
		offset = rand() % (nr_relax / 10); // +/- 10% 
		nr_relax = (offset % 2) ? (nr_relax - offset) : (nr_relax + offset);
		//printf("nr_relax: %lu\n", nr_relax);
		for (size_t i = 0; i < nr_relax; ++i) {
			cpu_relax();
		}

		(*iterations) += 1;
	}
}
